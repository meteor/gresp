{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "YAML dbird file schema",

  "definitions": {
    "optional_float": {
      "oneOf": [
        {
          "type": "number"
        },
        {
          "type": "null"
        }
      ]
    },
    "dbirddate": {
      "anyOf": [
        {
          "description": "Datetime format",
          "type": "string",
          "format": "date-time"
        },
        {
          "description": "Datetime format",
          "enum": ["present"]
        }
      ]
    },
    "float_with_uncertainty": {
      "type": "array",
      "items": [
        {
          "description": "The float value",
          "type": "number"
        },
        {
          "description": "Plus error value",
          "$ref": "#/definitions/optional_float"
        },
        {
          "description": "Minus error value",
          "$ref": "#/definitions/optional_float"
        }
      ]
    },
    "originating_organization": {
      "type": "array",
      "items": [
        {
          "description": "The institude that generate stationXML",
          "type": "string"
        },
        {
          "description": "The institude that generate stationXML contact",
          "type": "string",
          "format": "email"
        },
        {
          "description": "The institude that generate stationXML web site",
          "type": "string",
          "format": "uri"
        },
        {
          "description": "The institude that generate stationXML phone number",
          "type": "string"
        }
      ]
    },
    "author": {
      "type": "array",
      "items": [
        {
          "description": "Author's Names",
          "type": "array",
          "items": {"type": "string"}
        },
        {
          "description": "Author's agencies",
          "type": "array",
          "items": {"type": "string"}
        },
        {
          "description": "Author's email list",
          "type": "array",
          "items": {"type": "string", "format": "email"}
        },
        {
          "description": "Author's phone numbers",
          "type": "array",
          "items": {"type": "string"}
        }
      ]
    },
    "comment": {
      "type": "array",
      "items": [
        {
          "description": "Comment subject",
          "type": "string"
        },
        {
          "description": "Comment text",
          "type": "string"
        },
        {
          "description": "Comment begin date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Comment begin date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Comment authors",
          "type": "array",
          "items": {"$ref": "#/definitions/author"}
        },
        {
          "description": "Comment type",
          "enum": ["N", "S", "C"]
        }
      ]
    },
    "network": {
      "type": "array",
      "items": [
        {
          "description": "Network FDSN code",
          "type": "string",
          "minLength": 1,
          "maxLength": 2
        },
        {
          "description": "Network description",
          "type": "string"
        },
        {
          "description": "Network email contact",
          "type": "string",
          "format": "email"
        },
        {
          "description": "Network open date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Network close date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Network alternate code",
          "type": "string"
        },
        {
          "description": "Network DOI",
          "type": "string"
        },
        {
          "description": "Network comments",
          "type": "array",
          "items": {"$ref": "#/definitions/comment"}
        }
      ]
    },
    "network_list": {
      "type": "object",
      "required": ["comment", "network"],
      "additionalProperties": false,
      "properties": {
        "comment": {
          "type": "array",
          "items": {"$ref": "#/definitions/comment"}
        },
        "network": {
          "type": "array",
          "items": {"$ref": "#/definitions/network"}
        }
      }
    },
    "site": {
      "type": "object",
      "required": ["name"],
      "additionalProperties": false,
      "properties": {
        "name": {
          "description": "The station site name",
          "type": "string"
        },
        "description": {
          "description": "Actually this ",
          "type": "string"
        },
        "town": {
          "description": "The site town",
          "type": "string"
        },
        "county": {
          "description": "The site county",
          "type": "string"
        },
        "region": {
          "description": "The site region",
          "type": "string"
        },
        "country": {
          "description": "The site country",
          "type": "string"
        }
      }
    },
    "station_def": {
      "type": "array",
      "items": [
          {
            "description": "Station IRIS code",
            "type": "string",
            "minLength": 3,
            "maxLength": 5
          },
          {
            "description": "Station first install date",
            "$ref": "#/definitions/dbirddate"
          },
          {
            "description": "Station close date",
            "$ref": "#/definitions/dbirddate"
          },
          {
            "description": "Station latitude",
            "$ref": "#/definitions/float_with_uncertainty"
          },
          {
            "description": "Station longitude",
            "$ref": "#/definitions/float_with_uncertainty"
          },
          {
            "description": "Station altitude",
            "$ref": "#/definitions/float_with_uncertainty"
          },
          {
            "description": "Station toponyme",
            "type": "string"
          }
      ]
    },
    "sensor_location": {
      "type": "array",
      "items": [
        {
          "type": "string",
          "minLength": 0,
          "maxLength": 2
        },
        {
          "description": "Sensor location latitude",
          "$ref": "#/definitions/float_with_uncertainty"
        },
        {
          "description": "Sensor location longitude",
          "$ref": "#/definitions/float_with_uncertainty"
        },
        {
          "description": "Sensor location altitude",
          "$ref": "#/definitions/float_with_uncertainty"
        },
        {
          "description": "Sensor location depth",
          "type": "number"
        },
        {
          "description": "Sensor location vault",
          "type": "string"
        },
        {
          "description": "Sensor location geology",
          "type": "string"
        }
      ]
    },
    "sensor_stage": {
      "type": "array",
      "items": [
        {
          "description": "Sensor model",
          "type": "string"
        },
        {
          "description": "Sensor serial number",
          "type": "string"
        },
        {
          "description": "Sensor response file reference",
          "type": "string"
        },
        {
          "oneOf": [
            {
              "description": "Sensor azimuth",
              "type": "number",
              "minimum": 0,
              "maximum": 360
            },
            {
              "type": "null"
            }
          ]
        },
        {
          "oneOf": [
            {
              "description": "Sensor dip",
              "type": "number",
              "minimum": -90,
              "maximum": 90
            },
            {
              "type": "null"
            }
          ]
        }
      ]
    },
    "other_stage": {
      "type": "array",
      "items": [
        {
          "description": "Stage device model",
          "type": "string"
        },
        {
          "description": "Stage device serial number",
          "type": "string"
        },
        {
          "description": "Stage device response file reference",
          "type": "string"
        }
      ]
    },
    "channel": {
      "type": "array",
      "items": [
        {
          "description": "channel sensor location",
          "$ref": "#/definitions/sensor_location"
        },
        {
          "description": "Channel IRIS code",
          "type": "string",
          "minLength": 3,
          "maxLength": 3
        },
        {
          "description": "Channel sensor component",
          "$ref": "#/definitions/sensor_stage"
        },
        {
          "description": "Channel analog filters",
          "type": "array",
          "items": {"$ref": "#/definitions/other_stage"}
        },
        {
          "description": "Channel digitizer",
          "$ref": "#/definitions/other_stage"
        },
        {
          "description": "Channel digital filters",
          "type": "array",
          "items": {"$ref": "#/definitions/other_stage"}
        },
        {
          "description": "Channel flags",
          "type": "string"
        },
        {
          "description": "Channel data format",
          "type": "string"
        },
        {
          "description": "Channel open date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Channel close date",
          "$ref": "#/definitions/dbirddate"
        },
        {
          "description": "Channel sampling rate",
          "type": "number",
          "minimum": 0,
          "exclusiveMinimum": true
        },
        {
          "description": "Channel's network",
          "$ref": "#/definitions/network"
        },
        {
          "description": "Channel's comments",
          "type": "array",
          "items": {"$ref": "#/definitions/comment"}
        }
      ]
    },
    "station": {
      "type": "object",
      "required": ["definition", "owner", "site", "comment", "location",
                   "sensor", "ana_filter", "digitizer", "decimation", "channel"],
      "additionalProperties": false,
      "properties": {
        "definition": {"$ref": "#/definitions/station_def"},
        "contact": {
          "type": "string", 
          "format": "email"
        },
        "owner": {
          "description": "The station institude owner description",
          "type": "string"
        },
        "site": {
          "description": "The station site informations",
          "$ref": "#/definitions/site" 
        },
        "owner_website": {
          "description": "The station institude owner Web Site",
          "type": "string"
        },
        "comment": {
          "type": "array",
          "items": {"$ref": "#/definitions/comment"}
        },
        "location": {
          "type": "array",
          "items": {"$ref": "#/definitions/sensor_location"}
        },
        "sensor": {
          "type": "array",
          "items": {"$ref": "#/definitions/sensor_stage"}
        },
        "ana_filter":{
          "type": "array",
          "items": {"$ref": "#/definitions/other_stage"}
        },
        "digitizer":{
          "type": "array",
          "items": {"$ref": "#/definitions/other_stage"}
        },
        "decimation":{
          "type": "array",
          "items": {"$ref": "#/definitions/other_stage"}
        },
        "channel":{
          "type": "array",
          "items": {"$ref": "#/definitions/channel"}
        }
      }
    }
  },


  "type": "object",
  "required": ["version", "originating_organization", "author", "network",
               "station"],
  "additionalProperties": false,
  "properties": {
    "version": {"type": "number"},
    "originating_organization": {"$ref": "#/definitions/originating_organization"},
    "author": {
      "type": "array",
      "items": {"$ref": "#/definitions/author"}
    },
    "network": {"$ref": "#/definitions/network_list"},
    "station": {
      "type": "array",
      "items": {"$ref": "#/definitions/station"}
    }
  }
}
