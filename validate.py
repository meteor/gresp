#!/usr/bin/env python
import os
import sys
import json
import yaml
import argparse
from jsonschema import validate
from jsonschema import ValidationError, SchemaError


def load_file(filename, loader):
    if not os.path.exists(filename):
        sys.stderr.write("The input file %s does not exists\n" %
                         filename)
        sys.exit(1)

    try:
        config_file = open(filename)
        to_validate = loader(config_file)
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s\n" %
                         (filename, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Error parsing YAML in %s: %s\n" %
                         (filename, exception))
        sys.exit(1)
    return to_validate


def main():
    parser = argparse.ArgumentParser(description='Validate dbird YAML with a given JSON schema')

    input_file = parser.add_mutually_exclusive_group(required=True)
    input_file.add_argument("-y", "--yaml", help="Set YAML file to validate")
    input_file.add_argument("-j", "--json", help="Set JSON file to validate")
    parser.add_argument("--schema", help="Set schema file used to validate")
    parser.add_argument("-v", "--verbose", action="count", help="Increase verbosity level", default=0)

    option = parser.parse_args(sys.argv[1:])

    # Load file to validate
    if option.yaml is not None:
        to_validate = load_file(option.yaml, yaml.load)
    elif option.json is not None:
        to_validate = load_file(option.json, json.load)
    else:
        raise ValueError("Can't be here")

    # Load schema
    if not os.path.exists(option.schema):
        sys.stderr.write("The schema file %s does not exists\n" %
                         option.schema)
        sys.exit(1)
    try:
        schema_file = open(option.schema)
        schema = json.load(schema_file)
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s\n" %
                         (option.schema, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Error parsing JSON in %s: %s\n" %
                         (option.schema, exception))
        sys.exit(1)
    # Make validation
    validate(to_validate, schema)


if __name__ == "__main__":
    main()
