package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"os"
	"path"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/gdbird/oca/ydbird"
)

func main() {

	// Command line management
	ydbirdFile := flag.String("ydbird", "", "YAML dbird file input file")
	pzPath := flag.String("pz", "", "Set the PZ databank root directory")
	schemaRootDir := flag.String("schema", "", "Set the root schema directory")
	xmlFile := flag.String("o", "", "Set the name of stationXML output file")
	indent := flag.Bool("indent", false, "Enable output indent")
	verboseFlag := flag.Bool("v", false, "Set verbose flag")

	flag.Parse()

	if *ydbirdFile == "" {
		fmt.Printf("You must specify input file with -ydbird")
		os.Exit(1)
	}

	if *pzPath == "" {
		fmt.Printf("You must specify PZdatabank root directory with -pz")
		os.Exit(1)
	}

	if *schemaRootDir == "" {
		fmt.Printf("You must specify schema root directory with -schema")
		os.Exit(1)
	}

	if *xmlFile == "" {
		fmt.Printf("You must specify output file with -o\n")
		os.Exit(1)
	}

	// Loading sensor_file
	if *verboseFlag {
		fmt.Printf("Loading sensor_file\n")
	}
	sensorList, serr := dbirdv2.LoadSensorFile(path.Join(*pzPath, "sensor_list"),
		path.Join(*schemaRootDir, "sensor_list.json"))
	if serr != nil {
		fmt.Printf("Error loading sensor_file: %s\n", serr.Error())
		os.Exit(1)
	}

	dasList, derr := dbirdv2.LoadDasFile(path.Join(*pzPath, "das_list"),
		path.Join(*schemaRootDir, "das_list.json"))
	if derr != nil {
		fmt.Printf("Error loading das_file: %s\n", derr.Error())
		os.Exit(1)
	}

	// Loading unit_file
	if *verboseFlag {
		fmt.Printf("Loading unit_file\n")
	}
	unitList, uerr := dbirdv2.LoadUnitFile(path.Join(*pzPath, "unit_list"),
		path.Join(*schemaRootDir, "unit_list.json"))
	if uerr != nil {
		fmt.Printf("Error loading unit_file: %s\n", uerr.Error())
		os.Exit(1)
	}

	var dbirdFile dbirdv2.Dbird
	var lerr error
	if *verboseFlag {
		fmt.Printf("Loading ydbird file %s\n", *ydbirdFile)
	}
	dbirdFile, lerr = ydbird.Load(*ydbirdFile, *pzPath)
	if lerr != nil {
		fmt.Printf("Error loading dbird file: %s\n", lerr.Error())
		os.Exit(1)
	}

	// Generate stationXML if need
	converter := dbirdv2.NewStationXMLConverter(sensorList, dasList, unitList,
		*schemaRootDir)
	xmlContent := converter.Convert(dbirdFile)

	f, err := os.Create(*xmlFile)
	if err != nil {
		fmt.Printf("Error opening output file %s: %s", *xmlFile, err.Error())
		os.Exit(1)
	}
	defer f.Close()
	var output []byte
	if *indent {
		output, err = xml.MarshalIndent(xmlContent, "", "  ")
	} else {
		output, err = xml.Marshal(xmlContent)
	}
	if err != nil {
		fmt.Printf("Error generating XML: %s", err.Error())
		os.Exit(1)
	}
	f.Write([]byte("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"))
	_, err = f.Write([]byte(output))
	if err != nil {
		fmt.Printf("Error writing output file %s: %s", *xmlFile, err.Error())
		os.Exit(1)
	}
}
